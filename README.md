# Pip-tools project template

```bash
pip install pre-commit
pre-commit install

cd code
pip install -r requirements.txt

# (Optional): launch jupyter notebook if not using vscode
jupyter notebook
```
